from distutils.core import setup
from distutils.extension import Extension
from Cython.Distutils import build_ext

import numpy as np

setup(
    name = "PyDNA Transformations",
    include_dirs = [np.get_include()],
    cmdclass = {'build_ext': build_ext},
    ext_modules = [Extension("PyDNA", 
                             ["PyDNA.pyx"],
                             extra_compile_args=["-O3"])]
)
