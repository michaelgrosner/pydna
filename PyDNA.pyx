import numpy as np
cimport numpy as np

import cython
cimport cython

from libc.stdlib cimport free
from cpython cimport PyObject, Py_INCREF

from libc.math cimport sin, cos, atan2, acos, sqrt

DTYPE = np.float64
ctypedef np.float64_t DTYPE_t

cdef double pi, dpi
pi = acos(-1.0)
dpi = 1.0/pi # So python doesn't have to check for ZeroDivisionError

@cython.wraparound(False)
@cython.nonecheck(False)
@cython.boundscheck(False)
cpdef np.ndarray [DTYPE_t, ndim=1, mode='c'] bp_to_step(np.ndarray[DTYPE_t, ndim=2, mode='c'] W):
    cdef double cosgamma, gamma, phi, omega, sgcp, omega2_minus_phi
    cdef double sm, cm, sp, cp, sg, cg
    cdef np.ndarray [DTYPE_t, ndim=1, mode='c'] M = np.zeros(6)

    cosgamma = W[2,2]
    if cosgamma > 1.0: 
        cosgamma = 1.0
    elif cosgamma <= -1.0: 
        cosgamma = -1.0

    gamma = acos(cosgamma)

    sgcp = W[1,1]*W[0,2]-W[0,1]*W[1,2]

    if gamma == 0.0: 
        omega = -atan2(W[0,1], W[1,1])
    else:
        omega = atan2((W[2,1]*W[0,2]+sgcp*W[1,2]), (sgcp*W[0,2]-W[2,1]*W[1,2]))

    omega2_minus_phi = atan2(W[1,2], W[0,2])

    phi = omega/2.0 - omega2_minus_phi

    M[0] = gamma*sin(phi)*180.0*dpi
    M[1] = gamma*cos(phi)*180.0*dpi
    M[2] = omega*180.0*dpi

    sm = sin(omega/2.0-phi)
    cm = cos(omega/2.0-phi)
    sp = sin(phi)
    cp = cos(phi)
    sg = sin(gamma/2.0)
    cg = cos(gamma/2.0)

    M[3] = (cm*cg*cp-sm*sp)*W[0,3]+(sm*cg*cp+cm*sp)*W[1,3]-sg*cp*W[2,3]
    M[4] = (-cm*cg*sp-sm*cp)*W[0,3]+(-sm*cg*sp+cm*cp)*W[1,3]+sg*sp*W[2,3]
    M[5] = (cm*sg)*W[0,3]+(sm*sg)*W[1,3]+cg*W[2,3]

    return M

@cython.wraparound(False)
@cython.nonecheck(False)
@cython.boundscheck(False)
cpdef np.ndarray [DTYPE_t, ndim=2, mode='c'] step_to_bp(np.ndarray[DTYPE_t, ndim=1, mode='c'] tp):
    cdef double gamma, phi, omega, sp, cp, sm, cm, sg, cg, t1, t2, t3
    cdef np.ndarray [DTYPE_t, ndim=2, mode='c'] M = np.empty((4,4), dtype=DTYPE)

    t1 = tp[0]*pi/180.0
    t2 = tp[1]*pi/180.0
    t3 = tp[2]*pi/180.0

    gamma = sqrt(t1*t1+t2*t2)
    phi = atan2(t1,t2)
    omega = t3

    sp = sin(omega/2+phi)
    cp = cos(omega/2+phi)
    sm = sin(omega/2-phi)
    cm = cos(omega/2-phi)
    sg = sin(gamma)
    cg = cos(gamma)

    M[0,0] = cm*cg*cp-sm*sp
    M[0,1] = -cm*cg*sp-sm*cp
    M[0,2] = cm*sg
    M[1,0] = sm*cg*cp+cm*sp
    M[1,1] = -sm*cg*sp+cm*cp
    M[1,2] = sm*sg
    M[2,0] = -sg*cp
    M[2,1] = sg*sp
    M[2,2] = cg
    M[3,0] = 0
    M[3,1] = 0
    M[3,2] = 0
    M[3,3] = 1

    sp = sin(phi)
    cp = cos(phi)
    sg = sin(gamma/2)
    cg = cos(gamma/2)

    M[0,3] = tp[3]*(cm*cg*cp-sm*sp) + tp[4]*(-cm*cg*sp-sm*cp) + tp[5]*(cm*sg)
    M[1,3] = tp[3]*(sm*cg*cp+cm*sp) + tp[4]*(-sm*cg*sp+cm*cp) + tp[5]*(sm*sg)
    M[2,3] = tp[3]*(-sg*cp) + tp[4]*(sg*sp) + tp[5]*(cg)

    return M
